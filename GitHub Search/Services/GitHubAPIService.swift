//
//  GitHubAPIService.swift
//  GitHub Search
//
//  Created by Sohel Seedat on 19/10/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit
import Alamofire

enum ServiceError: Error {
	case urlError(reason: String)
	case statusCodeError(reason: String)
	case objectSerializationError(reason: String)
	case modelDecodeError(reason: String)
}

class GitHubAPIService {
	static let sharedInstance = GitHubAPIService()
	private init() {} //This prevents others from using the default '()' initializer for this class.
	
	let resultLimit = 30
	private let searchRepositoriesURL = "https://api.github.com/search/repositories?q="
	
	// This will fetch the JSON from the given url
	private func fetchJSON(requestURL: String, completionHandler: @escaping (Data?, Error?) -> ()) {
		Alamofire.request(requestURL).responseJSON { response in
		
			guard response.result.value as? [String: Any] != nil else {
				let error = ServiceError.urlError(reason: "Error while fetching JSON")
				completionHandler(nil, error)
				return
			}
			
			// Check if the request was successful
			guard response.result.isSuccess else {
				let error = ServiceError.urlError(reason: "Error while fetching JSON")
				completionHandler(nil, error)
				return
			}
			
			// Check if the request was successful
			guard response.response?.statusCode == 200 else {
				let error = ServiceError.statusCodeError(reason: "Error code \(response.response!.statusCode) while fetching JSON" )
				completionHandler(nil, error)
				return
			}
			
			// Check if there is any data
			guard response.result.value != nil else {
				let error = ServiceError.objectSerializationError(reason: "Invalid JSON received from the service")
				completionHandler(nil, error)
				return
			}

			completionHandler(response.data!, nil)
		}
	}
	
	// This will fetch the repositories 
	func fetchRepositories(searchText: String, pageNumber: Int? = 0, completionHandler: @escaping (SearchResult?, Error?) -> ()) {
		let searchURL = searchRepositoriesURL + searchText.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)! + "+in:name" + "&page=\(pageNumber!)"
		
		fetchJSON(requestURL: searchURL) { (data, error) in
			if (error == nil) {
				let decoder = JSONDecoder()
				decoder.dateDecodingStrategy = .iso8601
				
				do {
					let searchResult = try decoder.decode(SearchResult.self, from: data!)
					completionHandler(searchResult, nil)
				} catch {
					let error = ServiceError.modelDecodeError(reason: "Failed to convert data to Models")
					completionHandler(nil, error)
				}
			}
		}
	}
	
	func fetchReadMe(repository: Repository, completionHandler: @escaping (Readme?, Error?) -> ()) {
		// GET /repos/:owner/:repo/readme
		
		guard let repoName = repository.name else {
			let error = ServiceError.modelDecodeError(reason: "No Repo Name")
			completionHandler(nil, error)
			return
		}
		
		guard let login = repository.owner?.login else {
			let error = ServiceError.modelDecodeError(reason: "No Owner Login")
			completionHandler(nil, error)
			return
		}
		
		let readmeURL = "https://api.github.com/repos/\(login)/\(repoName)/readme"
		
		self.fetchJSON(requestURL: readmeURL) { (data, error) in
			if (error == nil) {
				let decoder = JSONDecoder()
				decoder.dateDecodingStrategy = .iso8601
				
				do {
					let readmeResult = try decoder.decode(Readme.self, from: data!)
					completionHandler(readmeResult, nil)
				} catch {
					let error = ServiceError.modelDecodeError(reason: "Failed to convert data to Models")
					completionHandler(nil, error)
				}
			} else {
				completionHandler(nil, error!)
			}
		}
	}
}
