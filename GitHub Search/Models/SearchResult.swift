//
//  SearchResult.swift
//  GitHub Search
//
//  Created by Sohel Seedat on 19/10/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit

struct SearchResult: Codable {
	let items: [Repository]?
	let incomplete_results: Int?
	let total_count: Int?
}
