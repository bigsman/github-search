//
//  Owner.swift
//  GitHub Search
//
//  Created by Sohel Seedat on 20/10/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit

struct Owner: Codable {
	var id: Int?
	var login: String?
	var avatar_url: String?
	var gravatar_id: String?
}
