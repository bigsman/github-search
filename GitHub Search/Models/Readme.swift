//
//  Readme.swift
//  GitHub Search
//
//  Created by Sohel Seedat on 21/10/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit

struct Readme: Codable {
	var name: String?
	var url: String?
	var html_url: String?
	var git_url: String?
	var download_url: String?
	var content: String?
	var encoding: String?
}
