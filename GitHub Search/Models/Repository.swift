//
//  Repository.swift
//  GitHub Search
//
//  Created by Sohel Seedat on 19/10/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit

struct Repository: Codable {
	var id: Int?
	var name: String?
	var full_name: String?
	var description: String?
	var created_at: Date?
	var updated_at: Date?
	var url: String?
	var language: String?
	var git_url: String?
	var stargazers_count: Int?
	var watcher_count: Int?
	var forks_count: Int?
	var open_issues_count: Int?
	var owner: Owner?
}
