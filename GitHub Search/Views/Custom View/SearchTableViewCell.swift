//
//  SearchTableViewCell.swift
//  GitHub Search
//
//  Created by Sohel Seedat on 19/10/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {
	
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var languageLabel: UILabel!
	@IBOutlet weak var descriptionLabel: UILabel!
	
    override func awakeFromNib() {
        super.awakeFromNib()
		
		self.selectionStyle = .none
		self.nameLabel.textColor = UIColor.mainOrange
    }
	
	func configure(repository: Repository) {
		if let name = repository.name {
			self.nameLabel.text = name
		} else {
			self.nameLabel.text = "N/A"
		}
		
		if let language = repository.language {
			self.languageLabel.text = language
		} else {
			self.languageLabel.text = "N/A"
		}
		
		if let description = repository.description {
			self.descriptionLabel.text = description
		} else {
			self.descriptionLabel.text = "No description"
		}
	}
}


