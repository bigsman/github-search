//
//  Extensions.swift
//  GitHub Search
//
//  Created by Sohel Seedat on 23/10/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit

extension UIViewController {
	
	func alert(message: String, title: String = "") {
		let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
		let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
		alertController.addAction(OKAction)
		self.present(alertController, animated: true, completion: nil)
	}
}

extension UIColor {
	static var mainOrange = UIColor(red:230/255, green:126/255, blue:34/255, alpha:1.0)
}
