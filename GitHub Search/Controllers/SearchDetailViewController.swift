//
//  SearchDetailViewController.swift
//  GitHub Search
//
//  Created by Sohel Seedat on 19/10/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit
import WebKit

class SearchDetailViewController: UIViewController, WKNavigationDelegate {
	
	// MARK: - Outlets
	@IBOutlet weak var issuesView: UIView!
	@IBOutlet weak var starsView: UIView!
	@IBOutlet weak var forksView: UIView!
	@IBOutlet weak var issuesLabel: UILabel!
	@IBOutlet weak var starsLabel: UILabel!
	@IBOutlet weak var forksLabel: UILabel!
	@IBOutlet weak var infoLabel: UILabel!
	@IBOutlet weak var contentView: UIView!
	
	// MARK: - Properties
	var repository: Repository!
	var readme: Readme?
	var webView: WKWebView!
	
	// MARK: - Services
	let gitHubAPIService = GitHubAPIService.sharedInstance
	
	// MARK: - View Controller
	override func loadView() {
		super.loadView()
		self.webView = WKWebView()
		self.webView.navigationDelegate = self
		self.view.addSubview(self.webView)
	}

    override func viewDidLoad() {
        super.viewDidLoad()
		
		setupView()
		setupData()
		fetchReadMe()
	}
	
	func setupView() {
		self.webView.frame = CGRect(x: 0, y: 114, width: self.view.frame.size.width, height: self.view.frame.size.height)
		
		issuesView.backgroundColor = UIColor.mainOrange
		starsView.backgroundColor = UIColor.mainOrange
		forksView.backgroundColor = UIColor.mainOrange
	}
	
	func setupData() {
		if let name = self.repository.name {
			self.title = name
		} else {
			self.title = "Repository Details"
		}
		
		if let issueCount = self.repository.open_issues_count {
			self.issuesLabel.text = String(issueCount)
		}
		
		if let stars = self.repository.stargazers_count {
			self.starsLabel.text = String(stars)
		}
		
		if let forks = self.repository.forks_count {
			self.forksLabel.text = String(forks)
		}
		
		if let info = self.repository.description {
			self.infoLabel.text = info
		} else {
			self.infoLabel.text = "No description"
		}
	}
	
	func fetchReadMe() {
		gitHubAPIService.fetchReadMe(repository: self.repository) { (readme, error) in
			if (error == nil) {
				self.readme = readme!
				
				if let urlString = self.readme?.html_url {
					let url = URL(string: urlString)
					self.webView.load(URLRequest(url: url!))
					self.webView.allowsBackForwardNavigationGestures = true
				}
			} else {
				self.alert(message: error!.localizedDescription, title: "ReadMe Error")
			}
		}
	}
}
