//
//  SearchViewController.swift
//  GitHub Search
//
//  Created by Sohel Seedat on 18/10/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit

enum State {
	case noData
	case loaded
	case noResults
	case error
}

class SearchViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {

	// MARK: - Outlets
	@IBOutlet weak var noDataView: UIView!
	@IBOutlet weak var noResultsView: UIView!
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var headerView: UIView!
	@IBOutlet weak var resultsFoundLabel: UILabel!
	@IBOutlet weak var searchBar: UISearchBar!
	
	// MARK: - Properties
	var searchResult: SearchResult?
	var repositories = [Repository]()
	var pageCount = 0
	var searchTerm = ""
	var state: State = .noData {
		didSet {
			switch state {
			case .noData:
				noDataView.isHidden = false
				tableView.isHidden = true
				noResultsView.isHidden = true
			case .loaded:
				noDataView.isHidden = true
				tableView.isHidden = false
				noResultsView.isHidden = true
				self.tableView.reloadData()
			case .noResults:
				noDataView.isHidden = true
				tableView.isHidden = true
				noResultsView.isHidden = false
			case .error:
				noDataView.isHidden = true
				tableView.isHidden = true
				noResultsView.isHidden = true
			}
		}
	}
	
	// MARK: - Services
	let gitHubAPIService = GitHubAPIService.sharedInstance
	
	// MARK: - View Controller
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		self.title = "GitHub Search"
		self.headerView.backgroundColor = UIColor.mainOrange
		setupSearchBar()
    }
	
	private func setupSearchBar() {
		searchBar.delegate = self
		searchBar.barStyle = .default
		searchBar.isTranslucent = false
		searchBar.barTintColor = UIColor.mainOrange
		searchBar.layer.borderWidth = 10
		searchBar.layer.borderColor = UIColor.mainOrange.cgColor
	}
	
	func fetchRepositories(searchText: String, pageNumber: Int? = 0) {
		gitHubAPIService.fetchRepositories(searchText: searchText.lowercased(), pageNumber: pageNumber) { (result, error) in
			// Check for backend errors
			if let error = error {
				self.alert(message: error.localizedDescription, title: "API Error")
				self.state = .noResults
				return
			}
			
			// Check Search Result is valid
			guard let searchResult = result else {
				print("error")
				return
			}
			
			// Check Repo is valid
			guard let repoItems = searchResult.items else {
				print("error")
				return
			}
			
			// No Repos returned
			guard repoItems.count > 0 else {
				self.alert(message: "No Repositories Found", title: "API Error")
				self.state = .noResults
				return
			}
			
			self.searchResult = searchResult
			self.repositories.append(contentsOf: repoItems)
			self.resultsFoundLabel.text = "\(searchResult.total_count!) results found"
			self.state = .loaded
		}
	}
	
	func fetchMoreRepositories() {
		if let searchResult = searchResult {
			if ((gitHubAPIService.resultLimit * self.pageCount) > searchResult.total_count!) {
				print("No more results")
				return
			}
		}
		
		self.pageCount += 1
		fetchRepositories(searchText: self.searchTerm, pageNumber: self.pageCount)
	}
	
	// MARK: - SearchBar Delegate

	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		guard let searchText = searchBar.text, !searchBar.text!.isEmpty else {
			return
		}
		
		if let _ = self.searchResult {
			//resetSearch()
		}
		
		self.searchTerm = searchText
		fetchRepositories(searchText: searchText)
		searchBar.resignFirstResponder()
	}
	
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		if (searchBar.text?.count == 0) {
			resetSearch()
		}
	}
	
	func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
		resetSearch()
	}
	
	func resetSearch() {
		self.repositories.removeAll()
		self.searchBar.text?.removeAll()
		self.state = .noData
		self.pageCount = 0
	}
	
	// MARK: - Tableview Delegate
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.repositories.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let repository = self.repositories[indexPath.row]
		let cell = tableView.dequeueReusableCell(withIdentifier: "RepositoryCell", for: indexPath) as! SearchTableViewCell
		cell.configure(repository: repository)
		return cell
	}
	
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		// check if user has got to the bottom of the list
		if (indexPath.row == self.repositories.count - 1) {
			fetchMoreRepositories()
		}
	}
	
	// MARK: - Segue
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if (segue.identifier == "SearchDetailSegueID") {
			let selectedRepo = self.repositories[self.tableView.indexPathForSelectedRow!.row]
			let detailVC = segue.destination as! SearchDetailViewController
			detailVC.repository = selectedRepo
		}
	}

}

