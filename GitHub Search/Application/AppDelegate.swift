//
//  AppDelegate.swift
//  GitHub Search
//
//  Created by Sohel Seedat on 18/10/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

		UIApplication.shared.statusBarStyle = .lightContent
		
		UINavigationBar.appearance().barTintColor = UIColor.mainOrange
		UINavigationBar.appearance().tintColor = UIColor.white
		UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
		UINavigationBar.appearance().shadowImage = UIImage()
		UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
		
		return true
	}
}

